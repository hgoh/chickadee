#!/usr/bin/env python
import sys
sys.path.append("..")
import chickadee
import numpy as np

# a symmetric three-hinged arch.
example1 = chickadee.frame2D("ex_hapl")
#
length =  1.0
height =  1.0
load   = -0.5
N      =  10
x = np.linspace(-length,0,N)
y = (1.0-(x/length)**2)*height

ii = 0
pM = example1.addNode(x[ii],y[ii])
example1.addConstraint(pM,1,1,0)
for ii in range(1,N):
    pN = example1.addNode(x[ii],y[ii])
    e = example1.addElement(pM,pN,1.0,1.0)
    pM = pN
example1.addConstraint(pN,1,0,0)
example1.addNodalLoad(pN,0.0,load,0.0)
# Start analysis.
example1.run()
# Plot structure and nodal deformation.
example1.plotResult()
# Print result on a file.
example1.printResult()


# a symmetric three-hinged arch.
example2 = chickadee.frame2D("ex_hadl")
#
length =  1.0
height =  1.0
load   = -1.0
N      =  10
x = np.linspace(-length,0,N)
y = (1.0-(x/length)**2)*height

ii = 0
pM = example2.addNode(x[ii],y[ii])
example2.addConstraint(pM,1,1,0)
for ii in range(1,N):
    pN = example2.addNode(x[ii],y[ii])
    e = example2.addElement(pM,pN,1.0,1.0)
    L = np.sqrt((x[ii]-x[ii-1])*(x[ii]-x[ii-1])+(y[ii]-y[ii-1])*(y[ii]-y[ii-1]))
    scaledLoad = load/L*(x[ii]-x[ii-1])
    example2.addElementalDistLoad(e,0.0,0.0,scaledLoad,scaledLoad)
    pM = pN
example2.addConstraint(pN,1,0,0)
# Start analysis.
example2.run()
# Plot structure and nodal deformation.
example2.plotResult()
# Print result on a file.
example2.printResult()




