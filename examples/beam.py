#!/usr/bin/env python
import sys
sys.path.append("..") 
import chickadee

# a frame without axial deformation
# Inintialize a chickadee project.
example = chickadee.frame2D("ex_beam")
# Add a node at (x,y) = (0,0). The function returns the node tag at p1.
p1 = example.addNode(0.0,0.0)
p2 = example.addNode(0.5,0.0)
p3 = example.addNode(1.0,0.0)
# Add an element with two node tazgs and E, I, A. Omit A to disable axial deformation.
e1 = example.addElement(p1,p2,1.0,1.0)
e2 = example.addElement(p2,p3,1.0,1.0)
# Add a constraint with a node tag and flags for x, y, and rotational constraints (0 - free, 1 - fixed).
example.addConstraint(p1,1,1,0)
example.addConstraint(p3,0,1,0)
# Add a nodal load  with an node tag, x, y, and rotational forces.
example.addNodalLoad(p2,0.0,-1.0,0.0)
# Start analysis.
example.run()
# Plot structure and nodal deformation.
example.plotResult()
# Print result on a file.
example.printResult()
