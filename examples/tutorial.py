#!/usr/bin/env python
import sys
sys.path.append("..") 
import chickadee

# a frame without axial deformation
# Inintialize a chickadee project.
example = chickadee.frame2D("ex_f")
# Add a node at (x,y) = (0,0). The function returns the node tag at p1.
p1 = example.addNode(-0.7,0.0)
p2 = example.addNode(0.0,1.0)
p3 = example.addNode(1.0,1.0)
p4 = example.addNode(1.0,0.0)
# Add an element with two node tazgs and E, I, A. Omit A to disable axial deformation.
e1 = example.addElement(p1,p2,1.0,1.0)
e2 = example.addElement(p2,p3,1.0,1.0)
e3 = example.addElement(p3,p4,1.0,1.0)
# Add a constraint with a node tag and flags for x, y, and rotational constraints (0 - free, 1 - fixed).
example.addConstraint(p1,1,1,1)
example.addConstraint(p4,1,1,0)
# Add a nodal load  with an node tag, x, y, and rotational forces.
example.addNodalLoad(p2,1.0,0.0,1.0)
# Add a point load on an element with an element tag, relative position, x, y, and rotational forces.
example.addElementalPointLoad(e2,0.7,0.0,-1.0,0.0)
# Add a distributed load on an element with an element tag, left-end and right-end values for x and y directions.
example.addElementalDistLoad(e2,0.0,0.0,-1.0,-2.0)
# Add a nodal displacement with a node tag, x, y, and rotational amount
example.addNodalDisplacement(p1,-1.0e-1,-1.0e-1,0.0)
# Start analysis.
example.run()
# Plot structure and nodal deformation.
example.plotResult()
# Print result on a file.
example.printResult()


# a frame with axial deformation
# Inintialize a chickadee project.
example2 = chickadee.frame2D("ex_fa")
# Add a node at (x,y) = (0,0). The function returns the node tag at p1.
p1 = example2.addNode(-0.7,0.0)
p2 = example2.addNode(0.0,1.0)
p3 = example2.addNode(1.0,1.0)
p4 = example2.addNode(1.0,0.0)
# Add an element with two node tags and E, I, A. Omit A to disable axial deformation.
e1 = example2.addElement(p1,p2,1.0,1.0,5.0e5)
e2 = example2.addElement(p2,p3,1.0,1.0,5.0e5)
e3 = example2.addElement(p3,p4,1.0,1.0,5.0e5)
# Add a constraint with a node tag and flags for x, y, and rotational constraints (0 - free, 1 - fixed).
example2.addConstraint(p1,1,1,1)
example2.addConstraint(p4,1,1,0)
# Add a nodal load  with an node tag, x, y, and rotational forces.
example2.addNodalLoad(p2,1.0,0.0,1.0)
# Add a point load on an element with an element tag, relative position, x, y, and rotational forces.
example2.addElementalPointLoad(e2,0.7,0.0,-1.0,0.0)
# Add a distributed load on an element with an element tag, left-end and right-end values for x and y directions.
example2.addElementalDistLoad(e2,0.0,0.0,-1.0,-2.0)
# Add a nodal displacement with a node tag, x, y, and rotational amount
example2.addNodalDisplacement(p1,-1.0e-1,-1.0e-1,0.0)
# Start analysis.
example2.run()
# Plot structure and nodal deformation.
example2.plotResult()
# Print result on a file.
example2.printResult()




