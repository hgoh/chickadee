#!/usr/bin/env python
################################################################################
# Two-dimensional structural analysis by Heedong Goh <heedong@snu.ac.kr>
################################################################################
from datetime import date
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.use('Agg')
################################################################################
__author__  = "Heedong Goh"
__version__ = "0.1"
__email__   = "heedong@snu.ac.kr"
__status__  = "Development"
################################################################################
class frame2D:
    ############################################################################
    ## private functions #######################################################
    ############################################################################
    def __init__(self,title="noname"):
        # Initialize all members below:
        self.title = title                   # title of the project
        self.analyzed = False                # True - post-computation, False - pre-calculation
        self.lockAxialDeformation = False    # True - beam element without axial deformation, False - frame element
        self.dof = 0                         # total number of DOFs (free+fixed)
        self.dofFree = 0                     # the number of free DOFs
        self.nodes = []                      # 
        self.constraints = []                # 
        self.elements = []
        self.elementProperties = []
        self.idX = []
        self.idY = []
        self.idR = []
        self.stiff = []
        self.load      = []
        self.disp      = []
        self.nodalLoad    = []
        self.nodalLoadVal = []
        self.nodalDisplacement    = []
        self.nodalDisplacementVal = []
        self.elementalPointLoad    = []
        self.elementalPointLoadVal = []
        self.elementalDistLoad    = []
        self.elementalDistLoadVal = []
        self.spring = []
        self.springVal = []
        return
    def __CalculateDOF__(self):
        nn  = len(self.nodes)
        self.idX = np.full(nn,-1,dtype=int)
        self.idY = np.full(nn,-1,dtype=int)
        self.idR = np.full(nn,-1,dtype=int)
        for ii in range(nn):
            cX = self.constraints[ii][0]
            cY = self.constraints[ii][1]
            cR = self.constraints[ii][2]
            if cX == False and self.idX[ii] < 0:
                self.idX[ii]  = self.dof
                self.dof += 1
            if cY == False and self.idY[ii] < 0:
                self.idY[ii]  = self.dof
                self.dof += 1
            if cR == False and self.idR[ii] < 0:
                self.idR[ii]  = self.dof
                self.dof += 1
        self.dofFree = self.dof
        for ii in range(nn):
            cX = self.constraints[ii][0]
            cY = self.constraints[ii][1]
            cR = self.constraints[ii][2]
            if cX == True and self.idX[ii] < 0:
                self.idX[ii]  = self.dof
                self.dof += 1
            if cY == True and self.idY[ii] < 0:
                self.idY[ii]  = self.dof
                self.dof += 1
            if cR == True and self.idR[ii] < 0:
                self.idR[ii]  = self.dof
                self.dof += 1
        return
    def __ElementStiff__(self,elementTag):
        xA   = self.nodes[self.elements[elementTag][0]][0]
        yA   = self.nodes[self.elements[elementTag][0]][1]
        xB   = self.nodes[self.elements[elementTag][1]][0]
        yB   = self.nodes[self.elements[elementTag][1]][1]
        E    = self.elementProperties[elementTag][0]
        I    = self.elementProperties[elementTag][1]
        A    = self.elementProperties[elementTag][2]
        L    = np.sqrt( (xB-xA)*(xB-xA) + (yB-yA)*(yB-yA) )
        ke   = np.asarray(
            [[   A,         0.0,      0.0,   -A,         0.0,      0.0 ],
             [ 0.0,  12.0*I/L/L,  6.0*I/L,  0.0, -12.0*I/L/L,  6.0*I/L ],
             [ 0.0,     6.0*I/L,    4.0*I,  0.0,    -6.0*I/L,    2.0*I ],
             [  -A,         0.0,      0.0,    A,         0.0,      0.0 ],
             [ 0.0, -12.0*I/L/L, -6.0*I/L,  0.0,  12.0*I/L/L, -6.0*I/L ],
             [ 0.0,     6.0*I/L,    2.0*I,  0.0,    -6.0*I/L,    4.0*I ]],
            dtype=np.double)
        ke  *= E/L
        cost = (xB-xA)/L
        sint = (yB-yA)/L
        rot  = np.asarray(
            [[  cost, sint, 0.0,   0.0,  0.0, 0.0 ],
             [ -sint, cost, 0.0,   0.0,  0.0, 0.0 ],
             [   0.0,  0.0, 1.0,   0.0,  0.0, 0.0 ],
             [   0.0,  0.0, 0.0,  cost, sint, 0.0 ],
             [   0.0,  0.0, 0.0, -sint, cost, 0.0 ],
             [   0.0,  0.0, 0.0,   0.0,  0.0, 1.0 ]],
            dtype=np.double)
        ke   = np.matmul(np.transpose(rot),np.matmul(ke,rot))
        return ke
    def __NodalLoadVector__(self):
        cload = np.zeros(self.dof,dtype=np.double)
        nitem = len(self.nodalLoad)
        for ii in range(nitem):
            cload[ self.idX[self.nodalLoad[ii]] ] += self.nodalLoadVal[ii][0]
            cload[ self.idY[self.nodalLoad[ii]] ] += self.nodalLoadVal[ii][1]
            cload[ self.idR[self.nodalLoad[ii]] ] += self.nodalLoadVal[ii][2]
        return cload
    def __ElementalLoadVector__(self):
        cload = np.zeros(self.dof,dtype=np.double)
        nitem = len(self.elementalPointLoad)
        for ii in range(nitem):
            elementTag = self.elementalPointLoad[ii]
            pos = self.elementalPointLoadVal[ii][0]
            loadGlobal = np.asarray(
                [ self.elementalPointLoadVal[ii][1],
                  self.elementalPointLoadVal[ii][2],
                  self.elementalPointLoadVal[ii][3] ],
                dtype=np.double)
            xA   = self.nodes[self.elements[elementTag][0]][0]
            yA   = self.nodes[self.elements[elementTag][0]][1]
            xB   = self.nodes[self.elements[elementTag][1]][0]
            yB   = self.nodes[self.elements[elementTag][1]][1]
            L    = np.sqrt( (xB-xA)*(xB-xA) + (yB-yA)*(yB-yA) )
            cost = (xB-xA)/L
            sint = (yB-yA)/L
            rot  = np.asarray(
                [[  cost, sint, 0.0 ],
                 [ -sint, cost, 0.0 ],
                 [   0.0,  0.0, 1.0 ]],
                dtype=np.double)
            loadLocal = np.matmul(rot,loadGlobal)
            fX = loadLocal[0]
            fY = loadLocal[1]
            fR = loadLocal[2]
            a = pos * L
            b = (1.0-pos) * L
            nodeALocal = np.asarray([  fX*b/L,
                                       fY*b*b*(3.0*a+b)/L/L/L -   6.0*fR*a*b/L/L/L,
                                       fY*a*b*b/L/L           - fR*b*(2.0*a-b)/L/L ],
                                    dtype=np.double)
            nodeBLocal = np.asarray([  fX*a/L,
                                       fY*a*a*(a+3.0*b)/L/L/L +   6.0*fR*a*b/L/L/L,
                                      -fY*a*a*b/L/L           - fR*a*(2.0*b-a)/L/L ],
                                    dtype=np.double)
            nodeAGlobal = np.matmul(np.transpose(rot),nodeALocal)
            nodeBGlobal = np.matmul(np.transpose(rot),nodeBLocal)
            cload[ self.idX[self.elements[elementTag][0]] ] += nodeAGlobal[0]
            cload[ self.idY[self.elements[elementTag][0]] ] += nodeAGlobal[1]
            cload[ self.idR[self.elements[elementTag][0]] ] += nodeAGlobal[2]
            cload[ self.idX[self.elements[elementTag][1]] ] += nodeBGlobal[0]
            cload[ self.idY[self.elements[elementTag][1]] ] += nodeBGlobal[1]
            cload[ self.idR[self.elements[elementTag][1]] ] += nodeBGlobal[2]
        nitem = len(self.elementalDistLoad)
        for ii in range(nitem):
            elementTag = self.elementalDistLoad[ii]
            loadGlobalFirst = np.asarray(
                [ self.elementalDistLoadVal[ii][0],
                  self.elementalDistLoadVal[ii][2],
                  self.elementalDistLoadVal[ii][4] ],
                dtype=np.double)
            loadGlobalSecond = np.asarray(
                [ self.elementalDistLoadVal[ii][1],
                  self.elementalDistLoadVal[ii][3],
                  self.elementalDistLoadVal[ii][5] ],
                dtype=np.double)
            xA   = self.nodes[self.elements[elementTag][0]][0]
            yA   = self.nodes[self.elements[elementTag][0]][1]
            xB   = self.nodes[self.elements[elementTag][1]][0]
            yB   = self.nodes[self.elements[elementTag][1]][1]
            L    = np.sqrt( (xB-xA)*(xB-xA) + (yB-yA)*(yB-yA) )
            cost = (xB-xA)/L
            sint = (yB-yA)/L
            rot  = np.asarray(
                [[  cost, sint, 0.0 ], 
                 [ -sint, cost, 0.0 ],
                 [   0.0,  0.0, 1.0 ]],
                dtype=np.double)
            loadLocalFirst  = np.matmul(rot,loadGlobalFirst)
            loadLocalSecond = np.matmul(rot,loadGlobalSecond)
            fXFirst = loadLocalFirst[0]
            fYFirst = loadLocalFirst[1]
            fRFirst = loadLocalFirst[2]
            fXDiff = loadLocalSecond[0] - fXFirst
            fYDiff = loadLocalSecond[1] - fYFirst
            fRDiff = loadLocalSecond[2] - fRFirst
            nodeALocal = np.asarray([  0.5*fXFirst*L + 1.0/6.0*fXDiff*L,
                                       0.5*fYFirst*L + 3.0/20.0*fYDiff*L,
                                       1.0/12.0*fYFirst*L*L + 1.0/30.0*fYDiff*L*L + fRFirst],
                                    dtype=np.double)
            nodeBLocal = np.asarray([  0.5*fXFirst*L + 2.0/6.0*fXDiff*L,
                                       0.5*fYFirst*L + 7.0/20.0*fYDiff*L,
                                      -1.0/12.0*fYFirst*L*L - 1.0/20.0*fYDiff*L*L + fRFirst + fRDiff],
                                    dtype=np.double)
            nodeAGlobal = np.matmul(np.transpose(rot),nodeALocal)
            nodeBGlobal = np.matmul(np.transpose(rot),nodeBLocal)
            cload[ self.idX[self.elements[elementTag][0]] ] += nodeAGlobal[0]
            cload[ self.idY[self.elements[elementTag][0]] ] += nodeAGlobal[1]
            cload[ self.idR[self.elements[elementTag][0]] ] += nodeAGlobal[2]
            cload[ self.idX[self.elements[elementTag][1]] ] += nodeBGlobal[0]
            cload[ self.idY[self.elements[elementTag][1]] ] += nodeBGlobal[1]
            cload[ self.idR[self.elements[elementTag][1]] ] += nodeBGlobal[2]
        return cload
    def __DisplacementVector__(self):
        self.disp = np.zeros(self.dof,dtype=np.double)
        nitem = len(self.nodalDisplacement)
        for ii in range(nitem):
            self.disp[ self.idX[self.nodalDisplacement[ii]] ] = self.nodalDisplacementVal[ii][0]
            self.disp[ self.idY[self.nodalDisplacement[ii]] ] = self.nodalDisplacementVal[ii][1]
            self.disp[ self.idR[self.nodalDisplacement[ii]] ] = self.nodalDisplacementVal[ii][2]
        return
    def __Assemble__(self,ke,elementTag):
        loc = [ self.idX[self.elements[elementTag][0]],
                self.idY[self.elements[elementTag][0]],
                self.idR[self.elements[elementTag][0]],
                self.idX[self.elements[elementTag][1]],
                self.idY[self.elements[elementTag][1]],
                self.idR[self.elements[elementTag][1]] ]
        edof = len(ke)
        for ii in range(edof):
            for jj in range(edof):
                self.stiff[loc[ii],loc[jj]] += ke[ii,jj]
        return
    def __AssembleSpring__(self):
        for ii in range(len(self.spring)):
            locX = self.idX[self.spring[ii]]
            locY = self.idY[self.spring[ii]]
            locR = self.idR[self.spring[ii]]
            self.stiff[locX,locX] += self.springVal[ii][0]
            self.stiff[locY,locY] += self.springVal[ii][1]
            self.stiff[locR,locR] += self.springVal[ii][2]
        return
    def __LockAxialDeformation__(self):
        small = 1.0e-13                 # 
        ne = len(self.elements)
        rmat  = np.zeros((self.dof,self.dof),dtype=np.double)
        rload = np.zeros(self.dof,dtype=np.double)
        cnt = 0
        for elementTag in range(ne):
            A = self.elementProperties[elementTag][2]
            if A <= 0:
                nodeA = self.elements[elementTag][0]
                nodeB = self.elements[elementTag][1]
                dofXA = self.idX[nodeA]
                dofYA = self.idY[nodeA]
                dofXB = self.idX[nodeB]
                dofYB = self.idY[nodeB]
                if dofXA >= self.dofFree and dofYA >= self.dofFree and dofXB >= self.dofFree and dofYB >= self.dofFree:
                    # Normally, this case should be avoided from the input.
                    print("Warning: removing redundant constraints; please check your input.")
                    continue
                xA    = self.nodes[nodeA][0]
                yA    = self.nodes[nodeA][1]
                xB    = self.nodes[nodeB][0]
                yB    = self.nodes[nodeB][1]
                L     = np.sqrt( (xB-xA)*(xB-xA) + (yB-yA)*(yB-yA) )
                cost  = (xB-xA)/L
                sint  = (yB-yA)/L
                rmat[cnt,dofXA] =  cost
                rmat[cnt,dofXB] = -cost
                rmat[cnt,dofYA] =  sint
                rmat[cnt,dofYB] = -sint
                if np.linalg.norm(rmat[cnt,:self.dofFree]) < small:
                    # Normally, this case should be avoided from the input.
                    print("Warning: removing redundant constraints; please check your input and reactions.")
                    rmat[cnt,:self.dofFree] = 0.0
                    continue
                #
                try:
                    loc = self.nodalDisplacement.index(nodeA)
                    rload[cnt] -= cost*self.nodalDisplacementVal[loc][0] + sint*self.nodalDisplacementVal[loc][1]
                except:
                    pass
                try:
                    loc = self.nodalDisplacement.index(nodeB)
                    rload[cnt] += cost*self.nodalDisplacementVal[loc][0] + sint*self.nodalDisplacementVal[loc][1]
                except:
                    pass
                cnt = cnt + 1
        return rmat[:cnt,:],rload[:cnt]
    ############################################################################
    ## public functions ########################################################
    ############################################################################
    def addNode(self,x,y):
        self.nodes.append([x,y])
        self.constraints.append([False,False,False])
        return len(self.nodes)-1 
    def addElement(self,nodeTagFirst,nodeTagSecond,E,I,A=-1.0):
        self.elements.append([nodeTagFirst,nodeTagSecond])
        self.elementProperties.append([E,I,A])
        if A <= 0:
            self.lockAxialDeformation = True
        return len(self.elements)-1
    def addConstraint(self,nodeTag,translationX,translationY,rotation):
        self.constraints[nodeTag] = [bool(translationX),bool(translationY),bool(rotation)]
        return 
    def addNodalLoad(self,nodeTag,fX,fY,fR):
        self.nodalLoad.append(nodeTag)
        self.nodalLoadVal.append([fX,fY,fR])
        return
    def addElementalPointLoad(self,elementTag,position,fX,fY,fR):
        self.elementalPointLoad.append(elementTag)
        self.elementalPointLoadVal.append([position,fX,fY,fR])
        return
    def addElementalDistLoad(self,elementTag,fXFirst,fXSecond,fYFrist,fYSecond,fRFrist=0.0,fRSecond=0.0):
        self.elementalDistLoad.append(elementTag)
        self.elementalDistLoadVal.append([fXFirst,fXSecond,fYFrist,fYSecond,fRFrist,fRSecond])
        return 
    def addNodalDisplacement(self,nodeTag,dX,dY,dR):
        self.nodalDisplacement.append(nodeTag)
        self.nodalDisplacementVal.append([dX,dY,dR])
        return
    def addSpring(self,nodeTag,kx,ky,kr):
        self.spring.append(nodeTag)
        self.springVal.append([kx,ky,kr])
        return
    def run(self):
        ne = len(self.elements)
        self.__CalculateDOF__()
        self.stiff = np.zeros((self.dof,self.dof), dtype=np.double)
        self.load = np.zeros(self.dof,dtype=np.double)
        pload = self.__NodalLoadVector__()
        eload = self.__ElementalLoadVector__()
        self.load = pload + eload
        self.__DisplacementVector__()
        for ii in range(ne):
            ke = self.__ElementStiff__(ii)
            self.__Assemble__(ke,ii)
        self.__AssembleSpring__()
        loadFree = self.load[:self.dofFree] - np.matmul(self.stiff[:self.dofFree,self.dofFree:],self.disp[self.dofFree:])
        #
        rmat,rload = self.__LockAxialDeformation__()
        nlag = rmat.shape[0]
        LK  = np.zeros((self.dofFree+nlag,self.dofFree+nlag),dtype=np.double)
        LF  = np.zeros(self.dofFree+nlag,dtype=np.double)
        gdisp = np.zeros(self.dofFree+nlag,dtype=np.double)
        LK[:self.dofFree, :self.dofFree ] = self.stiff[:self.dofFree,:self.dofFree]
        LK[:self.dofFree,  self.dofFree:] = np.transpose(rmat[:,:self.dofFree])
        LK[ self.dofFree:,:self.dofFree:] = rmat[:,:self.dofFree]
        LF[:self.dofFree] = loadFree
        LF[self.dofFree:] = rload
        gdisp = np.linalg.solve(LK,LF)
        self.disp[:self.dofFree]  = gdisp[:self.dofFree]
        self.load[self.dofFree:]  = np.matmul(self.stiff[self.dofFree:,:self.dofFree],self.disp[:self.dofFree])
        self.load[self.dofFree:] += np.matmul(self.stiff[self.dofFree:,self.dofFree:],self.disp[self.dofFree:])
        self.load[self.dofFree:] += np.matmul(np.transpose(rmat[:,self.dofFree:]),gdisp[self.dofFree:])
        self.load -= eload
        self.load -= pload
        #
        self.analyzed = True
        return 0
    def plotResult(self,figsize=(8.6,8.6),dpi=100,fileFormat=".png"):
        if self.analyzed == False:
            print(f"Not anlaylized")
            return 1
        ftitle = self.title+"_out"+fileFormat
        plt.rcParams['font.family'] = 'serif'
        plt.rcParams['font.sans-serif'] = ['Times New Roman']
        plt.rcParams['mathtext.fontset'] = 'cm'
        plt.rcParams['mathtext.rm'] = 'serif'
        plt.rcParams['font.size']=30
        fig, ax = plt.subplots(figsize=figsize,dpi=dpi)
        arrowSize = 0.05*np.max(np.max(np.abs(self.nodes)))
        for enodes in self.elements:
            ax.plot([self.nodes[enodes[0]][0],self.nodes[enodes[1]][0]],
                    [self.nodes[enodes[0]][1],self.nodes[enodes[1]][1]],
                    '-o',color='darkgray',lw=4,zorder=0)
        for ii in range(len(self.nodes)):
            newX = self.nodes[ii][0]+self.disp[self.idX[ii]]
            newY = self.nodes[ii][1]+self.disp[self.idY[ii]]
            ang  = self.disp[self.idR[ii]]
            cost = np.cos(ang)
            sint = np.sin(ang)
            ax.plot([newX],[newY],'o',color='black',markersize=8,zorder=1)
            ax.arrow(newX,newY,arrowSize*cost,arrowSize*sint,width=0.005,color='black',zorder=1)
            ax.arrow(newX,newY,-arrowSize*sint,arrowSize*cost,width=0.005,color='black',zorder=1)
        ax.set_aspect('equal')
        fig.tight_layout()
        fig.savefig(ftitle)
        plt.close()
        return 
    def printResult(self,fileFormat=".txt"):
        if self.analyzed == False:
            print(f"Not anlaylized")
            return 1
        ftitle = self.title+"_out"+fileFormat
        fout = open(ftitle,"w")
        fout.write(f"title: {self.title:s}\n")
        fout.write(f"date: {date.today()}\n")
        fout.write(f"\n* Structure *\n")
        fout.write(f"nodes (tag, x coordinate, y coordinate, x_constraint, y_constraint, rot_constraint):\n")
        for tag in range(len(self.nodes)):
            fout.write(f"{tag:d}\t{self.nodes[tag][0]:e}\t{self.nodes[tag][1]:e}\t")
            fout.write(f"{self.constraints[tag][0]:d}\t{self.constraints[tag][1]:d}\t{self.constraints[tag][2]:d}\n")
        fout.write(f"elements (tag, first node tag, second node tag, E, I, A):\n")
        for tag in range(len(self.elements)):
            fout.write(f"{tag:d}\t{self.elements[tag][0]:d}\t{self.elements[tag][1]:d}\t{self.elementProperties[tag][0]:20.10e}\t{self.elementProperties[tag][1]:20.10e}\t{self.elementProperties[tag][2]:20.10e}\n")
        fout.write(f"\n* External loads *\n")
        fout.write(f"nodal loads (tag, X, Y, rotation):\n")
        for tag in range(len(self.nodalLoad)):
            valA = self.nodalLoadVal[tag][0]
            valB = self.nodalLoadVal[tag][1]
            valC = self.nodalLoadVal[tag][2]
            fout.write(f"{tag:d}\t{valA:20.10e}\t{valB:20.10e}\t{valC:20.10e}\n")
        fout.write(f"elemental point loads (tag, relative position, X, Y, rotation):\n")
        for tag in range(len(self.elementalPointLoad)):
            valA = self.elementalPointLoadVal[tag][0]
            valB = self.elementalPointLoadVal[tag][1]
            valC = self.elementalPointLoadVal[tag][2]
            valD = self.elementalPointLoadVal[tag][3]
            fout.write(f"{tag:d}\t{valA:20.10e}\t{valB:20.10e}\t{valC:20.10e}\t{valD:20.10e}\n")
        fout.write(f"elemental distributed loads (tag, x first, x second, y first, y second, r first, r second):\n")
        for tag in range(len(self.elementalDistLoad)):
            valA = self.elementalDistLoadVal[tag][0]
            valB = self.elementalDistLoadVal[tag][1]
            valC = self.elementalDistLoadVal[tag][2]
            valD = self.elementalDistLoadVal[tag][3]
            valE = self.elementalDistLoadVal[tag][4]
            valF = self.elementalDistLoadVal[tag][5]
            fout.write(f"{tag:d}\t{valA:20.10e}\t{valB:20.10e}\t{valC:20.10e}\t{valD:20.10e}\t{valE:20.10e}\t{valF:20.10e}\n")
        fout.write(f"\n* Displacements and reactions *\n")
        fout.write(f"nodal displacements (tag, X, Y, rotation):\n")
        for tag in range(len(self.nodes)):
            valA = self.disp[self.idX[tag]]
            valB = self.disp[self.idY[tag]]
            valC = self.disp[self.idR[tag]]
            fout.write(f"{tag:d}\t{valA:20.10e}\t{valB:20.10e}\t{valC:20.10e}\n")
        fout.write(f"nodal reaction (tag, X, Y, rotation):\n")
        for tag in range(len(self.nodes)):
            valA = self.load[self.idX[tag]]
            valB = self.load[self.idY[tag]]
            valC = self.load[self.idR[tag]]
            fout.write(f"{tag:d}\t{valA:20.10e}\t{valB:20.10e}\t{valC:20.10e}\n")
        '''
        fout.write(f"\n* Member end forces in global coordinate (tag, XL, YL, ML, XR, YR, MR) no support for axial deformation locking yet*\n")
        for tag in range(len(self.elements)):
             valA = self.disp[ self.idX[self.elements[tag][0]] ]
             valB = self.disp[ self.idY[self.elements[tag][0]] ]
             valC = self.disp[ self.idR[self.elements[tag][0]] ]
             valD = self.disp[ self.idX[self.elements[tag][1]] ]
             valE = self.disp[ self.idY[self.elements[tag][1]] ]
             valF = self.disp[ self.idR[self.elements[tag][1]] ]
             disp = np.asarray( [valA,valB,valC,valD,valE,valF], dtype=np.double )
             ke = self.__ElementStiff__(tag)
             [valA,valB,valC,valD,valE,valF] = np.matmul(ke,disp)
             fout.write(f"{tag:d}\t{valA:20.10e}\t{valB:20.10e}\t{valC:20.10e}\t{valD:20.10e}\t{valE:20.10e}\t{valF:20.10e}\n")
        '''
        return 
################################################################################


if __name__ == '__main__':
    print("Chicka-dee-dee-dee!")

