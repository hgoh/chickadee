# chickadee 
a python script for two-dimensional structural analysis
by Heedong Goh <heedong@snu.ac.kr>

Please be careful when using the software, as it may have many bugs and errors that could lead to incorrect analyses.

Files:
- chickadee.py - contains frame2D class
- examples/tutorial.py - tutorial 

Required python packages:
- [numpy](https://numpy.org/)
- [matplotlib](https://matplotlib.org/)

TODO:
- add comments
- member end forces output
- hinged nodes; graphic input and output; temperature; fabrication error; cable element;  nonlinear deformation; and many more.

